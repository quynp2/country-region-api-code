package com.devcamp.countryregionrestapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CountryregionRestApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(CountryregionRestApiApplication.class, args);
	}

}
