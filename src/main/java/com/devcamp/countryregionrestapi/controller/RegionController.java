package com.devcamp.countryregionrestapi.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.countryregionrestapi.model.region;
import com.devcamp.countryregionrestapi.service.RegionService;

@RestController
@RequestMapping("/")
@CrossOrigin
public class RegionController {
    @Autowired
    private RegionService regionService;

    @GetMapping("/region-info")
    public region getRegionInfo(@RequestParam(required = true, name = "code") String regionCode) {
        region findRegion = regionService.filterRegion(regionCode);
        return findRegion;

    }

}
