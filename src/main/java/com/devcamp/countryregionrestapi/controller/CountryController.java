package com.devcamp.countryregionrestapi.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.countryregionrestapi.model.Country;
import com.devcamp.countryregionrestapi.service.CounryService;

@RestController
@CrossOrigin
@RequestMapping("/")
public class CountryController {
    @Autowired
    private CounryService countryService;

    @GetMapping("/countries")
    public ArrayList<Country> getAllCountry() {
        ArrayList<Country> allCountry = countryService.getAllCountry();
        return allCountry;

    }

    @GetMapping("/country-info")
    public Country getCountryInfo(@RequestParam(required = true, name = "code") String countryCode) {
        ArrayList<Country> allCountry = countryService.getAllCountry();
        Country findCountry = new Country();
        for (Country countryElement : allCountry) {
            if (countryElement.getCountryCode().equals(countryCode)) {
                findCountry = countryElement;
            }
        }
        return findCountry;

    }

    /**
     * @param index
     * @return
     */
    @GetMapping("/countries/{index}")
    public Country getCountryByIndex(@PathVariable(value = "index") int index) {
        Country results = new Country();
        List<Country> allCountry = countryService.getAllCountry();
        for (int i = 0; i < allCountry.size(); i++) {
            if (index == i) {
                results = allCountry.get(i);
            }

        }
        return results;

    }
}
