package com.devcamp.countryregionrestapi.service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.countryregionrestapi.model.Country;

@Service
public class CounryService {

    @Autowired
    private RegionService regionService;
    Country Vietnam = new Country("VN", "VietNam");
    Country Us = new Country("UK", "My");
    Country Russia = new Country("RU", "Nga");
    Country Japan = new Country("RU", "Nhat Ban");
    Country Singapore = new Country("RU", "Singapo");

    public ArrayList<Country> getAllCountry() {
        ArrayList<Country> allCountry = new ArrayList<>();
        Vietnam.setRegions(regionService.getRegionVietNam());
        Us.setRegions(regionService.getRegionUs());
        Russia.setRegions(regionService.getRegionRussia());
        Japan.setRegions(regionService.getRegionJapan());
        Singapore.setRegions(regionService.getRegionSingapore());

        allCountry.add(Vietnam);
        allCountry.add(Us);
        allCountry.add(Russia);
        allCountry.add(Japan);
        allCountry.add(Singapore);
        return allCountry;

    }

}
