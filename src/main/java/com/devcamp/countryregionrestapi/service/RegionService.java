package com.devcamp.countryregionrestapi.service;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.devcamp.countryregionrestapi.model.region;

@Service
public class RegionService {
    region hanoi = new region("HN", "Ha Noi");
    region tpHCM = new region("HCM", "TP Ho Chi Minh");
    region danang = new region("DN", "Da Nang");

    region newyork = new region("NY", "New York");
    region florida = new region("FL", "Florida");
    region texas = new region("TX", "Texas");

    region moscow = new region("MC", "Moscow");
    region kaluga = new region("KL", "Kaluga");
    region saintpeterburg = new region("SB", "Saintpeterburg");

    region Hokkaido = new region("HK", "Hokkaido");
    region Tohoku = new region("TH", "Tohoku");
    region Kanto = new region("KT", "Kanto");

    region JurongWest = new region("JW", "Jurong West");
    region Bedok = new region("BD", "Bedok");
    region Tampines = new region("TP", "Tampines");

    public ArrayList<region> getRegionVietNam() {
        ArrayList<region> regionofVietNam = new ArrayList<>();
        regionofVietNam.add(hanoi);
        regionofVietNam.add(tpHCM);
        regionofVietNam.add(danang);
        return regionofVietNam;
    }

    public ArrayList<region> getRegionUs() {
        ArrayList<region> regionofUs = new ArrayList<>();
        regionofUs.add(newyork);
        regionofUs.add(florida);
        regionofUs.add(texas);
        return regionofUs;
    }

    public ArrayList<region> getRegionRussia() {
        ArrayList<region> regionofRussia = new ArrayList<>();
        regionofRussia.add(moscow);
        regionofRussia.add(kaluga);
        regionofRussia.add(saintpeterburg);
        return regionofRussia;
    }

    public ArrayList<region> getRegionJapan() {
        ArrayList<region> regionofJapan = new ArrayList<>();
        regionofJapan.add(Hokkaido);
        regionofJapan.add(Tohoku);
        regionofJapan.add(Kanto);
        return regionofJapan;
    }

    public ArrayList<region> getRegionSingapore() {
        ArrayList<region> regionofSingapore = new ArrayList<>();
        regionofSingapore.add(JurongWest);
        regionofSingapore.add(Bedok);
        regionofSingapore.add(Tampines);
        return regionofSingapore;
    }

    /**
     * @param regionCode
     * @return
     */
    public region filterRegion(String regionCode) {
        ArrayList<region> regions = new ArrayList<region>();
        regions.add(hanoi);
        regions.add(tpHCM);
        regions.add(danang);
        regions.add(newyork);
        regions.add(florida);
        regions.add(texas);
        regions.add(moscow);
        regions.add(kaluga);
        regions.add(saintpeterburg);
        regions.add(Hokkaido);
        regions.add(Tohoku);
        regions.add(Kanto);
        regions.add(JurongWest);
        regions.add(Bedok);
        regions.add(Tampines);

        region findRegion = new region();

        for (region region : regions) {
            if (region.getRegionCode().equals(regionCode)) {
                findRegion = region;
            }

        }
        return findRegion;
    }

}
